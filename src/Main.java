import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

import java.io.File;

public class Main extends Application
{

    public static void main(String[] args) {
       launch(args);
    }

    double orgSceneX, orgSceneY;
    double orgTranslateX, orgTranslateY;

    @Override
    public void start(Stage primaryStage) throws Exception
    {
        primaryStage.setTitle("Scrambled Squares");
        Pane root = new Pane();
        Scene scene = new Scene(root, 1000, 1000, Color.WHITE);
        int imagenr = 1;
        double offset = 200;
        for(int row=0; row<3; row++){
            for(int col=0; col<3; col++){
                ImageView iv = getImageView( imagenr);
                double xTranslate = col * offset;
                double yTranslate = row * offset;
                iv.setTranslateX(xTranslate);
                iv.setTranslateY(yTranslate);
                root.getChildren().add(iv);
            imagenr++;
            }
        }
        primaryStage.setScene(scene);
        primaryStage.show();
    }


    private ImageView getImageView(int imageNumber){
        final MyImageView iView = new MyImageView(imageNumber);
        iView.setPreserveRatio(false);
        iView.setFitHeight(200);
        iView.setFitWidth(200);
        iView.addEventFilter(MouseEvent.MOUSE_CLICKED, e ->{
            if(e.getButton() == MouseButton.SECONDARY){ // respond to right-click
                iView.setRotate(90 + iView.getRotate());
            }
        });
        File file_ = new File(imageNumber + ".jpg");
        final Image image = new Image(file_.toURI().toString());
        iView.setImage(image);
        setEventHandlers(iView);
        return iView;
    }

    private void setEventHandlers(MyImageView iView)
    {
        iView.setOnMouseDragged(t -> {
            double offsetX = t.getSceneX() - orgSceneX;
            double offsetY = t.getSceneY() - orgSceneY;
            double newTranslateX = orgTranslateX + offsetX;
            double newTranslateY = orgTranslateY + offsetY;
                ((ImageView) (t.getSource())).setTranslateX(newTranslateX);
                ((ImageView) (t.getSource())).setTranslateY(newTranslateY);

        });
        iView.setOnMousePressed(t -> {
            orgSceneX = t.getSceneX();
            orgSceneY = t.getSceneY();
            orgTranslateX = ((ImageView)(t.getSource())).getTranslateX();
            orgTranslateY = ((ImageView)(t.getSource())).getTranslateY();
        });
    }
}
